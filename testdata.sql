SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `codegeek`
--

--
-- Дамп данных таблицы `cg_album`
--

INSERT INTO `cg_album` (`id`, `title`) VALUES
(1, 'The Sickness'),
(2, 'Believe');

--
-- Дамп данных таблицы `cg_album_music`
--

INSERT INTO `cg_album_music` (`album_id`, `post_id`) VALUES
(2, 2);

--
-- Дамп данных таблицы `cg_event`
--

INSERT INTO `cg_event` (`id`, `post_id`, `place_id`, `end_time`) VALUES
(1, 3, 1, '2016-10-07 23:00:00');

--
-- Дамп данных таблицы `cg_place`
--

INSERT INTO `cg_place` (`id`, `title`, `address`) VALUES
(1, 'Космонавт', 'Bronnitskaya ul., 24, г. Санкт-Петербург, 190013');

--
-- Дамп данных таблицы `cg_post`
--

INSERT INTO `cg_post` (`id`, `title`, `create_date`, `type_id`, `image`) VALUES
(1, 'Hard die', '1988-10-10 00:00:00', 1, 'data/images/1374192796.jpg'),
(2, 'Intoxication', '2002-09-17 00:00:00', 2, 'data/images/Disturbed_Believe.jpg'),
(3, 'Caliban Concert', '2016-10-07 20:00:00', 3, 'data/images/5209431.jpeg');

--
-- Дамп данных таблицы `cg_post_type`
--

INSERT INTO `cg_post_type` (`id`, `title`) VALUES
(1, 'Movie'),
(2, 'Song'),
(3, 'Event');

--
-- Дамп данных таблицы `cg_singer`
--

INSERT INTO `cg_singer` (`id`, `name`) VALUES
(1, 'Dan Donegan'),
(2, 'Mike Wengren'),
(3, 'David Draiman'),
(4, 'John Moyer');

--
-- Дамп данных таблицы `cg_singer_music`
--

INSERT INTO `cg_singer_music` (`singer_id`, `post_id`) VALUES
(1, 2),
(2, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
