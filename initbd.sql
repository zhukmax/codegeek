SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `codegeek`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cg_album`
--

CREATE TABLE `cg_album` (
  `id` int(10) NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cg_album_music`
--

CREATE TABLE `cg_album_music` (
  `album_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cg_event`
--

CREATE TABLE `cg_event` (
  `id` int(10) NOT NULL,
  `post_id` int(11) NOT NULL,
  `place_id` int(11) NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cg_panel`
--

CREATE TABLE `cg_panel` (
  `id` int(10) NOT NULL,
  `post_id` int(11) NOT NULL,
  `pub_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cg_place`
--

CREATE TABLE `cg_place` (
  `id` int(10) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cg_post`
--

CREATE TABLE `cg_post` (
  `id` int(10) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` datetime NOT NULL,
  `type_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cg_post_type`
--

CREATE TABLE `cg_post_type` (
  `id` int(10) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `cg_post_type`
--

INSERT INTO `cg_post_type` (`id`, `title`) VALUES
  (1, 'Movie'),
  (2, 'Song'),
  (3, 'Event');

-- --------------------------------------------------------

--
-- Структура таблицы `cg_singer`
--

CREATE TABLE `cg_singer` (
  `id` int(10) NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cg_singer_music`
--

CREATE TABLE `cg_singer_music` (
  `singer_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cg_album`
--
ALTER TABLE `cg_album`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cg_album_music`
--
ALTER TABLE `cg_album_music`
  ADD PRIMARY KEY (`album_id`,`post_id`),
  ADD KEY `album_id` (`album_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Индексы таблицы `cg_event`
--
ALTER TABLE `cg_event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `place_index` (`place_id`) USING BTREE,
  ADD KEY `post_id` (`post_id`);

--
-- Индексы таблицы `cg_panel`
--
ALTER TABLE `cg_panel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`);

--
-- Индексы таблицы `cg_place`
--
ALTER TABLE `cg_place`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cg_post`
--
ALTER TABLE `cg_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`);

--
-- Индексы таблицы `cg_post_type`
--
ALTER TABLE `cg_post_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cg_singer`
--
ALTER TABLE `cg_singer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cg_singer_music`
--
ALTER TABLE `cg_singer_music`
  ADD PRIMARY KEY (`singer_id`,`post_id`),
  ADD KEY `singer_id` (`singer_id`),
  ADD KEY `music_id` (`post_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cg_album`
--
ALTER TABLE `cg_album`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `cg_event`
--
ALTER TABLE `cg_event`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `cg_panel`
--
ALTER TABLE `cg_panel`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `cg_place`
--
ALTER TABLE `cg_place`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `cg_post`
--
ALTER TABLE `cg_post`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `cg_post_type`
--
ALTER TABLE `cg_post_type`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `cg_singer`
--
ALTER TABLE `cg_singer`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `cg_album_music`
--
ALTER TABLE `cg_album_music`
  ADD CONSTRAINT `cg_album_music_ibfk_1` FOREIGN KEY (`album_id`) REFERENCES `cg_album` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cg_album_music_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `cg_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `cg_event`
--
ALTER TABLE `cg_event`
  ADD CONSTRAINT `cg_event_ibfk_1` FOREIGN KEY (`place_id`) REFERENCES `cg_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cg_event_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `cg_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `cg_panel`
--
ALTER TABLE `cg_panel`
  ADD CONSTRAINT `cg_panel_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `cg_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `cg_post`
--
ALTER TABLE `cg_post`
  ADD CONSTRAINT `cg_post_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `cg_post_type` (`id`);

--
-- Ограничения внешнего ключа таблицы `cg_singer_music`
--
ALTER TABLE `cg_singer_music`
  ADD CONSTRAINT `cg_singer_music_ibfk_1` FOREIGN KEY (`singer_id`) REFERENCES `cg_singer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cg_singer_music_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `cg_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
