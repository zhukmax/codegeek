Yii 2 Тестовый проект из 2 частей
============================

1)  Так называемый генератор сущностей. Генератор - это консольный php код, который запускается из консоли linux/windows в папке проекта. 
И на основе подготовленных данных (набор фильмов, музыки, списка событий) для сущностей генерирует в случайном порядке записи в ленте 
сущностей. Генератор должен генерировать сущности(событие, музыку, кино) в ленту сущностей с интервалом в 10 сек.

2) Вывод ленты сущностей в браузер. 
Лента сущностей будет обновляться при нажатии кнопки “Обновить”. В ленту выводить сущности отсортированные по умолчанию, по дате добавления. 
В ленте должно отображаться не более 10 записей. Все остальные записи должны быть доступны через пейджер страниц. 
Пейджер страниц должен отрабатывать ajax-ом.
Все сущности в ленте должны фильтроваться по типу сущности (кино, музыка, событие) а так же по дате добавления. 
Фильтр должен быть отменяемым. Фильтрация так же должна срабатывать ajax-ом.

Выводится сводная статистика, такая как:
- общее количество сущностей в ленте;
- общее количество сущностей за сегодня;
- общее количество сущностей за сегодня сгруппированных по типам сущностей (фильмы, музыка, события)



DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.
