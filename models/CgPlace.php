<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cg_place".
 *
 * @property integer $id
 * @property string $title
 * @property string $address
 *
 * @property CgEvent[] $cgEvents
 */
class CgPlace extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cg_place';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'address'], 'required'],
            [['title', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'address' => 'Address',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCgEvents()
    {
        return $this->hasMany(CgEvent::className(), ['place_id' => 'id']);
    }
}
