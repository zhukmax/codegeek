<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cg_singer".
 *
 * @property integer $id
 * @property string $name
 *
 * @property CgPost[] $posts
 */
class CgSinger extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cg_singer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(CgPost::className(), ['id' => 'post_id'])->viaTable('cg_singer_music', ['singer_id' => 'id']);
    }
}
