<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cg_post_type".
 *
 * @property integer $id
 * @property string $title
 *
 * @property CgPost[] $cgPosts
 */
class CgPostType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cg_post_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCgPosts()
    {
        return $this->hasMany(CgPost::className(), ['type_id' => 'id']);
    }
}
