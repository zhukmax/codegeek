<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cg_event".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $place_id
 * @property string $end_time
 *
 * @property CgPlace $place
 * @property CgPost $post
 */
class CgEvent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cg_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'place_id', 'end_time'], 'required'],
            [['post_id', 'place_id'], 'integer'],
            [['end_time'], 'safe'],
            [['place_id'], 'exist', 'skipOnError' => true, 'targetClass' => CgPlace::className(), 'targetAttribute' => ['place_id' => 'id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => CgPost::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'place_id' => 'Place ID',
            'end_time' => 'End Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(CgPlace::className(), ['id' => 'place_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(CgPost::className(), ['id' => 'post_id']);
    }
}
