<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cg_album".
 *
 * @property integer $id
 * @property string $title
 *
 * @property CgPost[] $posts
 */
class CgAlbum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cg_album';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(CgPost::className(), ['id' => 'post_id'])->viaTable('cg_album_music', ['album_id' => 'id']);
    }
}
