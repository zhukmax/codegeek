<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cg_panel".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $pub_date
 *
 * @property CgPost $post
 */
class CgPanel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cg_panel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'pub_date'], 'required'],
            [['post_id'], 'integer'],
            [['pub_date'], 'safe'],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => CgPost::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'pub_date' => 'Pub Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(CgPost::className(), ['id' => 'post_id']);
    }
}
