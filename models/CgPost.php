<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cg_post".
 *
 * @property integer $id
 * @property string $title
 * @property string $create_date
 * @property integer $type_id
 * @property string $image
 *
 * @property CgAlbum[] $albums
 * @property CgEvent[] $cgEvents
 * @property CgPanel[] $cgPanels
 * @property CgPostType $type
 * @property CgSinger[] $singers
 */
class CgPost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cg_post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'create_date', 'type_id', 'image'], 'required'],
            [['create_date'], 'safe'],
            [['type_id'], 'integer'],
            [['title', 'image'], 'string', 'max' => 255],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CgPostType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'create_date' => 'Дата создания',
            'type_id' => 'Тип',
            'image' => 'Изображение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbums()
    {
        return $this->hasMany(CgAlbum::className(), ['id' => 'album_id'])->viaTable('cg_album_music', ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCgEvents()
    {
        return $this->hasMany(CgEvent::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCgPanels()
    {
        return $this->hasMany(CgPanel::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(CgPostType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSingers()
    {
        return $this->hasMany(CgSinger::className(), ['id' => 'singer_id'])->viaTable('cg_singer_music', ['post_id' => 'id']);
    }
}
