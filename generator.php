<?php
set_time_limit(0);
$content = true;
// Настройки подключения к БД из Yii2 достаем
$config = [
    'db' => require('./config/db.php'),
];

try {
    // Подключение к Базе дынных через PDO
    $conn = new PDO($config['db']['dsn'], $config['db']['username'], $config['db']['password']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Подготовим SQL запрос
    $sqlString = "SELECT post.id as id";
    $where = " WHERE post.id NOT IN (SELECT `post_id` FROM `cg_panel`) ";
    $limit = " ORDER BY RAND() LIMIT 1";
    $sqlString .= " FROM `cg_post` post ".$where.$limit;

    while($content) {
        // Выполняем запрос и передаем в $post массив с id новой записи
        // для ленты сущностей
        $stmt = $conn->prepare($sqlString);
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $post = $stmt->fetch();

        if (isset($post['id'])) {
            // Добавляем запись в ленту
            $sqlString = "INSERT INTO `cg_panel` (id, post_id) VALUES (NULL, ?)";
            $stmt = $conn->prepare($sqlString);
            $stmt->bindParam(1, $post['id']);
            $stmt->execute();
            echo "Add new post to panel #".$post['id'];

            // Подождем 10 секунд прежде чем снова запустить добавление
            sleep(10);
        } else {
            echo "No more posts";
            $content = false;
        }
    }
}
// Обработка Исключений, возникающих при ошибке подключения к БД
catch(PDOException $e)
{
    echo "Connection failed: " . $e->getMessage();
}

// Разрываем соединение с БД
$conn = null;