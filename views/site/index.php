<?php

/* @var $this yii\web\View */

use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->title = 'Code Geek Test';
?>
<div class="site-index">
    <?php
    NavBar::begin(['brandLabel' => 'Фильтры']);
    echo Nav::widget([
        'items' => [
            ['label' => 'Фильмы', 'linkOptions' => ['onclick' => 'myFunction("1")']],
            ['label' => 'Музыка', 'linkOptions' => ['onclick' => 'myFunction("2")']],
            ['label' => 'События', 'linkOptions' => ['onclick' => 'myFunction("3")']],
            ['label' => 'сбросить', 'url' => ['/']],
        ],
        'options' => ['class' => 'navbar-nav'],
    ]);
    NavBar::end();
    ?>

    <div class="body-content">

        <div class="row">
            <?php

            Pjax::begin();
            foreach ($panel as $p) {
                ?>
                <div class="col-lg-12 post">
                    <div class="col-lg-2">
                        <img src="<?=$p->post->image ?>" alt="<?=$p->post->title ?>" style="max-width: 100%">
                    </div>
                    <div class="col-lg-10">
                        <h2><?=$p->post->title ?></h2>

                        <?php
                            if ($p->post->type_id === 2) {
                        ?>
                                <p>Исполнители:
                                    <?php
                                    $singers = $p->post->singers;
                                    foreach ($singers as $singer) {
                                        echo $singer->name."; ";
                                    }
                                    ?>
                                </p>
                        <?php
                            } elseif ($p->post->type_id === 3) {
                        ?>
                                <p>
                                    <?php
                                    $event = $p->post->cgEvents[0];
                                    echo "<strong>".$event->place->title."</strong>; ".$event->place->address;
                                    ?>
                                </p>
                        <?php
                            }
                        ?>

                        <?php
                            if ($p->post->type_id !== 3) {
                        ?>
                                <p><?=date('d/m/Y', strtotime($p->post->create_date)) ?> </p>
                        <?php
                            } else {
                        ?>
                                <p><?=date('H:i d/m/Y', strtotime($p->post->create_date)) ?>
                                    -
                                    <?php
                                        $event = $p->post->cgEvents[0];
                                        echo date('H:i d/m/Y', strtotime($event->end_time));
                                    ?>
                                </p>
                        <?php
                            }
                        ?>
                    </div>
                </div>
                <?php
            }

            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
            Pjax::end();
            ?>
        </div>

    </div>
</div>

<?php
Modal::begin([
    'header' => '<h2>Статистика</h2>',
]);
?>

<dl>
    <dt>Общее количество сущностей в ленте:</dt>
    <dd><?=$stat['total'] ?></dd>

    <dt>Общее количество сущностей за сегодня:</dt>
    <dd><?=$stat['today'] ?></dd>

    <dt>Общее количество фильмов:</dt>
    <dd><?=$stat['movie'] ?></dd>

    <dt>Общее количество музыки:</dt>
    <dd><?=$stat['music'] ?></dd>

    <dt>Общее количество событий:</dt>
    <dd><?=$stat['events'] ?></dd>
</dl>

<?php
Modal::end();
?>
