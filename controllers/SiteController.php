<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\CgPanel;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Вывод ленты сущностей в браузер
     *
     * @return string
     */
    public function actionIndex()
    {
        $postData = Yii::$app->request->post();
        if (isset($postData['type'])) {
            $filter = true;
            $query = CgPanel::find()
                ->joinWith('post')
                ->where(['cg_post.type_id' => $postData['type']])
                ->orderBy('pub_date');
        } else {
            $filter = false;
            $query = CgPanel::find()->orderBy('pub_date')->with('post');
        }
        $countQuery = clone $query;

        // Данные для статистики
        $stat['total'] = CgPanel::find()->count();
        $stat['today'] = CgPanel::find()->where("pub_date >= CURDATE()")->count();
        $stat['movie'] = CgPanel::find()
            ->where("pub_date >= CURDATE()")
            ->joinWith('post')
            ->andWhere(['cg_post.type_id' => 1])
            ->count();
        $stat['music'] = CgPanel::find()
            ->where("pub_date >= CURDATE()")
            ->joinWith('post')
            ->andWhere(['cg_post.type_id' => 2])
            ->count();
        $stat['events'] = CgPanel::find()
            ->where("pub_date >= CURDATE()")
            ->joinWith('post')
            ->andWhere(['cg_post.type_id' => 3])
            ->count();

        // подключаем класс Pagination, выводим по 10 пунктов на страницу
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);

        $pages->pageSizeParam = false;
        $panel = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $data = [
            'panel' => $panel,
            'pages' => $pages,
            'stat' => $stat,
        ];

        if ($filter === false) {
            // Передаем данные в представление
            return $this->render('index', $data);
        } else {
            return $this->renderPartial('index', $data);
        }
    }

}
